FROM ubuntu:20.04

ENV TF_VER=1.7.0

WORKDIR /tmp

RUN apt-get update  
RUN apt-get install -y wget curl gnupg unzip git ca-certificates apt-transport-https lsb-release 

#Install Terraform
RUN wget https://releases.hashicorp.com/terraform/${TF_VER}/terraform_${TF_VER}_linux_amd64.zip
RUN unzip terraform_${TF_VER}_linux_amd64.zip
RUN mv terraform /usr/local/bin/

#Install Azure cli
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash

RUN rm -rf /tmp/*

RUN mkdir -p /tf/azure
WORKDIR /tf/azure

# COPY test.sh .
# RUN chmod 755 test.sh
